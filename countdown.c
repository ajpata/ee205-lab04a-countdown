///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author AJ Patalinghog <ajpata@hawaii.edu>
// @date   4 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h> //for the sleep() function


int main(int argc, char* argv[]) {

   struct tm   refTime = {0};
   char        refTimeStr [100];
   time_t      timer;
   struct tm * difference = {0};

   //Reference time:  Tue Jan 21 04:26:07 PM HST 2014
   refTime.tm_wday = 2;
   refTime.tm_mon = 0;
   refTime.tm_mday = 21;
   refTime.tm_hour = 16;
   refTime.tm_min = 26;
   refTime.tm_sec = 7;
   refTime.tm_year = 2014 - 1900;


   strftime(refTimeStr, 100, "Reference time:  %a %b %d %I:%M:%S %p %Z %Y", &refTime);
   printf("%s\n", refTimeStr);

   while(1) {
  
      timer = difftime(time(NULL), mktime(&refTime));

      difference = gmtime(&timer);

      printf("Years: %d  ", difference->tm_year - 70); //came from timer which is a time_t that started at 1970
      printf("Days: %d  ", difference->tm_yday);
      printf("Hours: %d  ", difference->tm_hour);
      printf("Minutes: %d  ", difference->tm_min);
      printf("Seconds: %d\n", difference->tm_sec);

      sleep(1);
   }
}
